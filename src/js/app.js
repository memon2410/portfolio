// Styles
import '../scss/app.scss'

// Import classes
import Cursor from './components/cursor'
import Pic from './layout/pic'

document.addEventListener('DOMContentLoaded', () => {
  if ('scrollRestoration' in history) {
    history.scrollRestoration = 'manual'
  }

  const pic = new Pic()
  const cursor = new Cursor()
})
