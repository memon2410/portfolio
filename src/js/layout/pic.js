import WebGLView from '../webgl/WebGLView'
import { gsap } from 'gsap/all'

class Pic {
  constructor () {
    this.initWebGL()
    this.addListeners()
    this.animate()
    this.resize()
  }

  initWebGL () {
    this.webgl = new WebGLView(this)
    document
      .querySelector('.wrapper')
      .appendChild(this.webgl.renderer.domElement)
  }

  addListeners () {
    this.handleAnimate = this.animate.bind(this)
    window.addEventListener('resize', this.resize.bind(this))
  }

  animate () {
    this.update()
    this.draw()

    this.raf = requestAnimationFrame(this.handleAnimate)
  }

  update () {
    if (this.webgl) {
      this.webgl.update()
    }
  }

  draw () {
    if (this.webgl) {
      this.webgl.draw()
    }
  }

  resize () {
    if (this.webgl) {
      this.webgl.resize()
    }
  }
}

export default Pic
