import isMobile from 'ismobilejs'

/** @function
 * @name constants: getDevice
 * @desc Returns the device type
 * @example
 * // mobile, tablet, desktop
 * @returns {string}
 */
const getDevice = () => {
  const mobile = isMobile(window.navigator).phone
  const tablet = isMobile(window.navigator).tablet
  if (mobile) return 'mobile'
  else if (tablet) return 'tablet'
  else return 'desktop'
}

/** @function
 * @name constants: getDevice
 * @desc Return the quality for the video
 * @example
 * // low, mid, high
 * @returns {string}
 */
const getQuality = () => {
  const device = getDevice()
  if (device == 'mobile') return 'low'
  else if (device == 'tablet') return 'mid'
  else return 'high'
}

/** @constant
 * @type {string}
 */
export const QUALITY = getQuality()

/** @constant
 * @type {string}
 */
export const DEVICE = getDevice()
