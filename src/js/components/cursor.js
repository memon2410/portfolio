import { throttle } from 'throttle-debounce'
import { gsap, Power2 } from 'gsap/all'

class Cursor {
  constructor () {
    this.onceTime = false
    this.scale = 0.75
    this.translation = {
      x: 1,
      y: 1
    }
    this.pos = {
      x: window.innerWidth / 2,
      y: window.innerHeight / 2,
      friction: 1
    }
    this.precision = 1

    this.tl = null
    this.container = null
    this.circle = null

    this.buttons = document.getElementsByTagName('button')
    this.refs = document.getElementsByTagName('a')

    this.initCursor()
  }

  setScale (scale, time) {
    gsap.to('#cursor-circle', time, {
      scaleX: scale,
      scaleY: scale,
      opacity: 1
    })
  }

  onMouseMove (event) {
    if (event.touches) event = event.touches[0]
    this.pos.x = event.clientX
    this.pos.y = event.clientY

    if (!this.onceTime) {
      this.onceTime = true
      this.setScale(0.2, 2)
    }
    this.render()
  }

  setup () {
    const body = document.getElementsByTagName('body')[0]
    this.container = document.createElement('div')
    body.appendChild(this.container)
    this.container.setAttribute('id', 'cursor')

    this.circle = document.createElement('div')
    this.container.appendChild(this.circle)
    this.circle.setAttribute('id', 'cursor-circle')
  }

  events () {
    const mouseMove = throttle(30, event => this.onMouseMove(event))

    document.addEventListener('mousemove', mouseMove, false)

    Array.from(this.buttons).map((current, index) => {
      current.addEventListener(
        'mouseenter',
        event => {
          this.setScale(1, 0.15)
        },
        false
      )
    })

    Array.from(this.buttons).map((current, index) => {
      current.addEventListener(
        'mouseleave',
        event => {
          this.setScale(0.15, 0.15)
        },
        false
      )
    })

    Array.from(this.refs).map((current, index) => {
      current.addEventListener(
        'mouseover',
        event => {
          this.setScale(1, 0.25)
        },
        false
      )
    })

    Array.from(this.refs).map((current, index) => {
      current.addEventListener(
        'mouseout',
        event => {
          this.setScale(0.15, 0.15)
        },
        false
      )
    })
  }

  initCursor () {
    this.events()
    this.setup()
  }

  render () {
    this.translation.x += (this.pos.x - this.translation.x) * this.pos.friction
    this.translation.y += (this.pos.y - this.translation.y) * this.pos.friction

    this.container.style.transform =
      'translate3d(' +
      this.translation.x.toFixed(this.precision) +
      'px ,' +
      this.translation.y.toFixed(this.precision) +
      'px, 0) scale(' +
      this.scale +
      ')'
  }
}

export default Cursor
