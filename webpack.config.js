const localServer = {
  path: 'localhost/public/',
  port: 3000
}

const path = require('path')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const ImageMinPlugin = require('imagemin-webpack-plugin').default
const __root = path.resolve(__dirname, '../')

const config = {
  entry: {
    app: './src/js/app.js'
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'public')
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      {
        test: /\.(png|gif|jpg|jpeg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'images/design/[name].[hash:6].[ext]',
              publicPath: '../',
              limit: 8192
            }
          }
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'fonts/[name].[hash:6].[ext]',
              publicPath: '../',
              limit: 8192
            }
          }
        ]
      },
      {
        test: /\.(glsl|frag|vert)$/,
        use: ['glslify-import-loader', 'raw-loader', 'glslify-loader']
      },
      {
        test: /three\/examples\/js/,
        use: 'imports-loader?THREE=three'
      }
    ]
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true
      }),
      new OptimizeCssAssetsPlugin({})
    ]
  },
  resolve: {
    alias: {
      'three-examples': path.join(__root, './node_modules/three/examples/js')
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      hash: false,
      filename: 'index.html',
      template: path.resolve(__dirname, 'src', 'index.html'),
      favicon: path.resolve(
        __dirname,
        'src',
        'images/content/favicon',
        'favicon.ico'
      )
    }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css'
    }),
    new ImageMinPlugin({ test: /\.(jpg|jpeg|png|gif|svg)$/i }),
    new CleanWebpackPlugin({
      cleanStaleWebpackAssets: false,
      verbose: true
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'images', 'content'),
        to: path.resolve(__dirname, 'public', 'images', 'content'),
        toType: 'dir'
      }
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', '', 'manifest.json'),
        to: path.resolve(__dirname, 'public', '', 'manifest.json'),
        toType: 'file'
      }
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', 'files', 'resume.pdf'),
        to: path.resolve(__dirname, 'public', 'files', 'resume.pdf'),
        toType: 'file'
      }
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', '', 'sitemap.xml'),
        to: path.resolve(__dirname, 'public', '', 'sitemap.xml'),
        toType: 'file'
      }
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', '', 'robots.txt'),
        to: path.resolve(__dirname, 'public', '', 'robots.txt'),
        toType: 'file'
      }
    ]),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src', '', 'humans.txt'),
        to: path.resolve(__dirname, 'public', '', 'humans.txt'),
        toType: 'file'
      }
    ])
  ]
}

module.exports = config
